package com.amh.coinwashmanager.exception;

public class CNoMemberPasswordException extends RuntimeException{

    // 1번, 생성자 3개만들 것임
    public CNoMemberPasswordException(String msg, Throwable t) {
        super(msg, t); //위에 두개 받아줄 것.
    }

    public CNoMemberPasswordException(String msg) {
        super(msg);
    }

    // 3번째는 빈바구니
    public CNoMemberPasswordException() {
        super();
    }
}
