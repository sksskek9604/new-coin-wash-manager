package com.amh.coinwashmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
