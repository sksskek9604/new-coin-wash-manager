package com.amh.coinwashmanager.repository;

import com.amh.coinwashmanager.entity.LoginUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginUserRepository extends JpaRepository<LoginUser, Long> {

    Optional<LoginUser> findByUsername(String username); // Q. 왜 findByPassword는 안할까?
    //A. 비밀번호 찾기같은건 서비스의 흐름을 생각해보자 (아이디, 본인인증 -> 비밀번호 바꾸는 흐름)

    long countByUsername(String username); // 이 유저네임이랑 일치하는 데이터가 몇개니?
    // 없으면 가입 가능, 있으면 중복이란 뜻이 되니 가입 안됨
}
