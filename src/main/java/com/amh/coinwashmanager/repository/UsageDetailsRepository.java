package com.amh.coinwashmanager.repository;

import com.amh.coinwashmanager.entity.UsageDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UsageDetailsRepository extends JpaRepository<UsageDetails, Long> {

    //리스트 가져와서 sql문처럼 해보자.
    List<UsageDetails> findAllByDateUsingGreaterThanEqualAndDateUsingLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
    // 이용시간을 전부 찾아오는데, 크거나 같고, 작거나 같은 id들 중, 내림차순 (시작 시간, 끝나는 시간)

}
