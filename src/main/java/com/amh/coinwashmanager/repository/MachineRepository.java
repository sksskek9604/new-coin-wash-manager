package com.amh.coinwashmanager.repository;

import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {

    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);

}
