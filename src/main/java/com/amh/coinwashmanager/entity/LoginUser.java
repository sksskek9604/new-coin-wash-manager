package com.amh.coinwashmanager.entity;

import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import com.amh.coinwashmanager.model.loginuser.LoginUserRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15, unique = true)
    private String username;

    @Column(nullable = false, length = 15)
    private String password;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    private LoginUser(LoginUserBuilder loginUserBuilder) {
        this.username = loginUserBuilder.username;
        this.password = loginUserBuilder.password;
        this.dateCreate = loginUserBuilder.dateCreate;
        this.dateUpdate = loginUserBuilder.dateUpdate;
    }

    public static class LoginUserBuilder implements CommonModelBuilder<LoginUser> {

        private final String username;
        private final String password;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;


        public LoginUserBuilder(LoginUserRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public LoginUser build() {
            return new LoginUser(this);
        }
    }
}
