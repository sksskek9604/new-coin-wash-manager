package com.amh.coinwashmanager.entity;

import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "machineId", nullable = false)
    private Machine machine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateUsing; //이용날짜가 아니라 이용시간일 경우

    private UsageDetails (UsageDetailsBuilder usageDetailsBuilder) {
        this.machine = usageDetailsBuilder.machine;
        this.member = usageDetailsBuilder.member;
        this.dateUsing = usageDetailsBuilder.dateUsing;

    }
    public static class UsageDetailsBuilder implements CommonModelBuilder<UsageDetails> {
        private final Machine machine;
        private final Member member;
        private final LocalDateTime dateUsing;

        public UsageDetailsBuilder(Machine machine, Member member, LocalDateTime dateUsing) {
            this.machine = machine;
            this.member = member;
            this.dateUsing = dateUsing;
        }

        @Override
        public UsageDetails build() {
            return new UsageDetails(this);
        }
    }

}
