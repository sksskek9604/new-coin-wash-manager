package com.amh.coinwashmanager.configure;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 전체라는 뜻
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(3000);
        //주소를 적으면 거기만 풀어준다는 것임 *는 전체 풀어준다고 한 것.
    //주소를 적으면 거기만 풀어준다는 것임 *는 전체 풀어준다고 한 것.
    }
}
