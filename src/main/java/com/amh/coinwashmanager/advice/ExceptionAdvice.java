package com.amh.coinwashmanager.advice;

import com.amh.coinwashmanager.enums.ResultCode;
import com.amh.coinwashmanager.exception.CMissingDataException;
import com.amh.coinwashmanager.exception.CNoMemberDataException;
import com.amh.coinwashmanager.exception.CNoMemberPasswordException;
import com.amh.coinwashmanager.model.common.CommonResult;
import com.amh.coinwashmanager.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) { //기본 비상구, 기본 실패하였습니다.
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) //사용자가 잘못이면 400번으로 보내줌
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoMemberDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 회원탈퇴 된 척 하기. + 회원정보 진짜로 없을 때
    protected CommonResult customException(HttpServletRequest request, CNoMemberDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_MEMBER_DATA);
    }

    @ExceptionHandler(CNoMemberPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST) // 회원 패스워드 불일치
    protected CommonResult customException(HttpServletRequest request, CNoMemberPasswordException e) {
        return ResponseService.getFailResult(ResultCode.NO_MEMBER_PASSWORD);
    }

}
