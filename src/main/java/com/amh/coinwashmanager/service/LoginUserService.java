package com.amh.coinwashmanager.service;


import com.amh.coinwashmanager.entity.LoginUser;
import com.amh.coinwashmanager.exception.CNoMemberDataException;
import com.amh.coinwashmanager.exception.CNoMemberPasswordException;
import com.amh.coinwashmanager.model.loginuser.LoginUserItem;
import com.amh.coinwashmanager.model.loginuser.LoginUserRequest;
import com.amh.coinwashmanager.model.loginuser.LoginUserResponse;
import com.amh.coinwashmanager.repository.LoginUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginUserService {

    private final LoginUserRepository loginUserRepository;

    private boolean isDuplicateUser(String username) {
        long dupCount = loginUserRepository.countByUsername(username);

        return dupCount >= 1;
    }

    // 레포지터리에 해놨는데 메소드로 굳이 왜뺐을까?
    // 어딘가에 회원가입할 때 생김새를 보면, 아이디/버튼이 있는데 '중복확인'라는 부분이 있죠
    // 그럴 때 '가입이 가능한 아이디입니다.', '이미 가입된 아이디입니다' 를 프론트가 쓰는 경우가 있음
    // 만약 나랑 친구랑 똑같은 아이디로 동시 회원가입하는데, 아이디가 없어서 가입이 가능하다하고 가입을 한다면..?
    // 내가 먼저 확인버튼을 눌렀다, 그럼 나는 가입이 성공했지만, 친구는 ..?
    // 백에서 체크를 안하게 되면, 그냥 exception 중에서 실패합니다로만 떨어지겠죠?
    // 그래서, '이미 가입된 아이디입니다.'라고 띄워줘야겠죠.


    /**
     * 회원 정보 등록하기
     * @param request 회원 정보 등록 폼
     */
    public void setLoginUser(LoginUserRequest request) {
        if (isDuplicateUser(request.getUsername())) throw new CNoMemberDataException();

        LoginUser loginUser = new LoginUser.LoginUserBuilder(request).build();
        loginUserRepository.save(loginUser);
    }

    /**
     * 로그인 하기
     * @param request 로그인 폼(* 회원가입 폼이 아이디와 패스워드만 받기에 똑같은 모델로 사용)
     * @return 로그인
     */

    public LoginUserResponse setLoginUserDoLogin(LoginUserRequest request) {
        LoginUser loginUser = loginUserRepository.findByUsername(request.getUsername()).orElseThrow(CNoMemberDataException::new);

        if (!loginUser.getPassword().equals(request.getPassword())) throw new CNoMemberPasswordException();
        return new LoginUserResponse.LoginUserResponseBuilder(loginUser).build();
    }

    /**
     * 회원 정보 불러오기
     * @param id 회원 시퀀스
     * @return 회원 정보 아이템
     */
    public LoginUserItem getLoginUser(long id) {
        LoginUser loginUser = loginUserRepository.findById(id).orElseThrow(CNoMemberDataException::new);
        return new LoginUserItem.LoginUserItemBuilder(loginUser).build();
    }
}
