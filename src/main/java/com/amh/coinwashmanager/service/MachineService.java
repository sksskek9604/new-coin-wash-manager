package com.amh.coinwashmanager.service;

import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.enums.MachineType;
import com.amh.coinwashmanager.exception.CMissingDataException;
import com.amh.coinwashmanager.model.common.ListResult;
import com.amh.coinwashmanager.model.machine.MachineDetail;
import com.amh.coinwashmanager.model.machine.MachineItem;
import com.amh.coinwashmanager.model.machine.MachineNameUpdateRequest;
import com.amh.coinwashmanager.model.machine.MachineRequest;
import com.amh.coinwashmanager.repository.MachineRepository;
import com.amh.coinwashmanager.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {

    private final MachineRepository machineRepository;


    /**
     * UsageDetails에서 사용하기 위한 메서드
     * @param id 기계 시퀀스
     * @return 기계
     */
    public Machine getMachineData(long id) {
        Machine machine= machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return machine;
    }


    /**
     * 기계 등록
     * @param request 기계 등록 폼
     */
    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    /**
     * 기계 정보 불러오기
     * @param id 기계 시퀀스
     * @return 기계 정보 아이템
     */
    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();
    }


    /**
     * 기계 정보 전체 불러오기
     * @return 기계 정보 리스트
     */
    public ListResult<MachineItem> getMachines() {
        List<Machine> machines = machineRepository.findAll();

        List<MachineItem> result = new LinkedList<>();

//        for (Machine machine : machines) {
//            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
//            result.add(addItem);
//        }

        machines.forEach(machine -> {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
        // 위에는 for문 아래에는 forEach를 썼습니다.
        // 코드모양이 더 예쁜건 어느쪽인가요? 아래쪽이 좀 더 명확한 코드이다.
        // 리스트들(machines)을 하나(each)씩 던져줘라(forEach), 그걸 machine이라고 부를게

        // forEach 쓰는 법
        // 쓰는 법: 1. 원본의 리스트 이름을 갖고와(리스트 변수 machines)
        // 2. 점을 찍고 forEach()를 가져와
        // 3. 한뭉텅이를 뭐라고 부를거냐, 헷갈리지 않게 machine이라고 부를게 하고 -> {} 모양도 같이 눌러줘요.
        // 4. 위 for문에서 두줄이었으니까, 길어지니 중괄호가 필요한거예요. 중괄호 안에서 enter!
        // 5. for문 안에 썼던 내용 그대로 써주면 됩니다. -끝-

    }

    /**
     * (기계타입) 기계 정보 불러오기
     * @param machineType 기계 타입
     * @return 기계타입에 따른 기계 정보 리스트
     */
    public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByIdDesc(machineType);

        List<MachineItem> result = new LinkedList<>();

        machines.forEach(machine -> {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기계 이름 수정
     * @param id 기계 시퀀스
     * @param request 기계 이름 폼
     */
    public void putMachineName(long id, MachineNameUpdateRequest request) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(request);
        machineRepository.save(machine);
    }

    /**
     * 기계 전체 정보 수정
     * @param id 기계 시퀀스
     * @param request 기계 수정 폼
     */
    public void putMachine(long id, MachineRequest request) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putMachine(request);
        machineRepository.save(machine);
    }

}
