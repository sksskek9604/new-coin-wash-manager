package com.amh.coinwashmanager.service;

import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.entity.Member;
import com.amh.coinwashmanager.entity.UsageDetails;
import com.amh.coinwashmanager.model.UsageDetailsItem;
import com.amh.coinwashmanager.model.common.ListResult;
import com.amh.coinwashmanager.repository.UsageDetailsRepository;
import com.amh.coinwashmanager.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {

    private final UsageDetailsRepository usageDetailsRepository;

    /**
     * 이용 하기 등록
     * @param member 회원
     * @param machine 기계
     * @param dateUsage 이용시간
     */
    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    /**
     * 기간 별 이용내역 조회하기
     * @param dateStart 조회 시작 날짜
     * @param dateEnd 조회 마지막 날짜
     * @return 기간 별 이용 내역 리스트
     */
    // 고객에서 부터도 박스가 없고, 박스로 받을 수 없으니까 따로 따로 넣어줘야함.(시작일/종료일)
    public ListResult<UsageDetailsItem> getUsageDetailsAll(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        ); // 0시 0분 0초 부터

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        ); // 23시 59분 59초

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsingGreaterThanEqualAndDateUsingLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetails1 -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetails1).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);
    }

}

