package com.amh.coinwashmanager.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {

    //회원가입시에만 필요한걸 생각해보자.
    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "회원 이름")
    private String memberName;

    @NotNull
    @Length(max = 20)
    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;

    @NotNull
    @ApiModelProperty(notes = "회원 생년월일")
    private LocalDate birthday;

}
