package com.amh.coinwashmanager.model.member;

import com.amh.coinwashmanager.entity.Member;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)

public class MemberItem {

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "회원 이름")
    private String memberName;
    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;
    @ApiModelProperty(notes = "회원 생년월일")
    private LocalDate birthday;
    @ApiModelProperty(notes = "회원가입 날짜")
    private LocalDateTime dateJoin; //회원가입날짜
    @ApiModelProperty(notes = "유효한 회원 여부")
    private String isEnable;
    @ApiModelProperty(notes = "회원 탈퇴날짜")
    private LocalDateTime dateWithdrawal;

    private MemberItem(MemberItemBuilder memberItemBuilder) {
        this.id = memberItemBuilder.id;
        this.memberName = memberItemBuilder.memberName;
        this.memberPhone = memberItemBuilder.memberPhone;
        this.birthday = memberItemBuilder.birthday;
        this.dateJoin = memberItemBuilder.dateJoin;
        this.isEnable = memberItemBuilder.isEnable;
        this.dateWithdrawal = memberItemBuilder.dateWithdrawal;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final LocalDateTime dateJoin; //회원가입날짜
        private final String isEnable;
        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder(Member member) {
            // isEnable은 삼항연산자 사용.
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.memberPhone = member.getMemberPhone();
            this.birthday = member.getBirthday();
            this.dateJoin = member.getDateJoin();
            this.isEnable = member.getIsEnable() ? "예" : "아니오";
            this.dateWithdrawal = member.getDateWithdrawal();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }

}
