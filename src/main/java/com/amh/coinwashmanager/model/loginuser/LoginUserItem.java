package com.amh.coinwashmanager.model.loginuser;

import com.amh.coinwashmanager.entity.LoginUser;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginUserItem {


    @ApiModelProperty(notes = "회원 시퀀스" )
    private Long id;

    @ApiModelProperty(notes = "회원 ID")
    private String username;

    @ApiModelProperty(notes = "회원 비밀번호")
    private String password;


    private LoginUserItem(LoginUserItemBuilder loginUserItemBuilder) {
        this.id = loginUserItemBuilder.id;
        this.username = loginUserItemBuilder.username;
        this.password = loginUserItemBuilder.password;
    }

    public static class LoginUserItemBuilder implements CommonModelBuilder<LoginUserItem> {

        private final Long id;
        private final String username;
        private final String password;

        public LoginUserItemBuilder(LoginUser loginUser) {
            this.id = loginUser.getId();
            this.username = loginUser.getUsername();
            this.password = loginUser.getPassword();
        }

        @Override
        public LoginUserItem build() {
            return new LoginUserItem(this);
        }
    }
}
