package com.amh.coinwashmanager.model.loginuser;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LoginUserRequest {

    @NotNull
    @Length(min = 8, max = 15)
    @ApiModelProperty(notes = "회원 ID")
    private String username;

    @NotNull
    @Length(min = 8, max = 15)
    @ApiModelProperty(notes = "회원 비밀번호")
    private String password;

}
