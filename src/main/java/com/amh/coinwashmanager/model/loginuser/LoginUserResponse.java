package com.amh.coinwashmanager.model.loginuser;

import com.amh.coinwashmanager.entity.LoginUser;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginUserResponse {

    private Long id;

    private LoginUserResponse(LoginUserResponseBuilder loginUserResponseBuilder) {
        this.id = loginUserResponseBuilder.id;
    }

    public static class LoginUserResponseBuilder implements CommonModelBuilder<LoginUserResponse> {

        private final Long id;

        public LoginUserResponseBuilder(LoginUser loginUser) {
            this.id = loginUser.getId();
        }

        @Override
        public LoginUserResponse build() {
            return new LoginUserResponse(this);
        }
    }
}
