package com.amh.coinwashmanager.model;

import com.amh.coinwashmanager.entity.UsageDetails;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {
    @ApiModelProperty(notes = "이용 내역 시퀀스")
    private Long usageDetailsId; // 1. id 여러개 들고올거니까 명확하게 이름
    @ApiModelProperty(notes = "이용 날짜")
    private LocalDateTime dateUsing; // 2. 이용내역을 두번째로 들고오기
    // 3. SQL에서 join했을 때 정보가 다같이 보였죠? 그때처럼 join을 해봅시다.
    // 4. machine쪽에서 가져올 것 보기
    @ApiModelProperty(notes = "머신 시퀀스")
    private Long machineId; // 머신 시퀀스
    @ApiModelProperty(notes = "머신타입과 이름")
    private String machineFullName; // 5. 머신 타입(세탁기, 건조기) + 머신 이름(ex: A열 B열 위치 이름)
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId; // 6. 멤버 아이디
    @ApiModelProperty(notes = "회원 이름")
    private String memberName; // 7. 고객 이름, 폰번호, 생년월일 함께 있으면 좋으니까 가지고오기
    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;
    @ApiModelProperty(notes = "회원 생년월일")
    private LocalDate birthday;
    @ApiModelProperty(notes = "회원 탈퇴여부")
    private Boolean memberIsEnable;
    @ApiModelProperty(notes = "회원 가입일")
    private LocalDateTime memberDateJoin;
    @ApiModelProperty(notes = "회원 탈퇴일")
    private LocalDateTime memberDateWithdrawal;

    private UsageDetailsItem(UsageDetailsItemBuilder builder) {

        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsing = builder.dateUsing;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.birthday = builder.birthday;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;

    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {

        private final Long usageDetailsId;
        private final LocalDateTime dateUsing;
        private final Long machineId; // 여기서부터는 빌더에 어떻게 넣어야할까? ->JPA가 자동으로 당겨와줬음. lazy했었죠?
        private final String machineFullName;
        private final Long memberId;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final Boolean memberIsEnable;
        private final LocalDateTime memberDateJoin;
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsing = usageDetails.getDateUsing();
            this.machineId = usageDetails.getMachine().getId(); // jpa가 당겨와주고, entity 에서 ManyToOne- lazy했던 것(필요할 때만 들고오기로 했음)
            this.machineFullName = usageDetails.getMachine().getMachineType().getName() + " " + usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getId();  // 멤버에서 다 끌고오기
            this.memberName = usageDetails.getMember().getMemberName();
            this.memberPhone = usageDetails.getMember().getMemberPhone();
            this.birthday = usageDetails.getMember().getBirthday();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }
}

