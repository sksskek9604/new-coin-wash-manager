package com.amh.coinwashmanager.model.machine;

import com.amh.coinwashmanager.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {

    @NotNull
    @Enumerated(EnumType.STRING) //length 필요없음
    @ApiModelProperty(notes = "머신 타입")
    private MachineType machineType;

    @NotNull
    @Length(max = 15)
    @ApiModelProperty(notes = "머신 이름")
    private String machineName;

    @NotNull
    @ApiModelProperty(notes = "머신 구매일")
    @DateTimeFormat
    private LocalDate datePurchase;
    // Todo LocalDate 입력 형식 swagger 확인시 오류, 확인 필요함. (yyyy-mm-dd)


    @NotNull
    @ApiModelProperty(notes = "머신 가격")
    private Double machinePrice;

}
