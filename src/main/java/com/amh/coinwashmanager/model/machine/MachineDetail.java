package com.amh.coinwashmanager.model.machine;


import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineDetail {

    @ApiModelProperty(notes = "머신 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "머신 타입명")
    private String machineTypeName;  //헷갈리니까 재가공할 때 이름 바꿔주기

    @ApiModelProperty(notes = "머신 이름")
    private String machineName;

    @ApiModelProperty(notes = "구매일")
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "가격")
    private Double machinePrice; //사실 프론트에서 바꿔서 보여줄 수 있기 때문에 Double로 가져와도 괜찮습니다.

    // 쿠팡 기준으로 볼때는 등록일/수정일 필요없지만,
    // 관리자모드일때는 필요하겠죠. 지금은 쿠팡 디테일이니까 안넣어요.
    private MachineDetail(MachineDetailBuilder builder) {
        this.id = builder.id;
        this.machineTypeName = builder.machineTypeName;
        this.machineName = builder.machineName;
        this.datePurchase = builder.datePurchase;
        this.machinePrice = builder.machinePrice;

    }
    public static class MachineDetailBuilder implements CommonModelBuilder<MachineDetail> {
        // 재가공한다는건 모든 값이 필수란 뜻이에요.
        private final Long id;
        private final String machineTypeName;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Double machinePrice;

        public MachineDetailBuilder(Machine machine) {
            // Machine을 받는 이유는 원본을 받아서 재가공하는거니까.
            this.id = machine.getId();
            this.machineTypeName = machine.getMachineType().getName();
            this.machineName = machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
            this.machinePrice = machine.getMachinePrice();
        }

        @Override
        public MachineDetail build() {
            return new MachineDetail(this);
        }
    }

}
