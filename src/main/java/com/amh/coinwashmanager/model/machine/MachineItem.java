package com.amh.coinwashmanager.model.machine;

import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {


    @ApiModelProperty(notes = "머신 시퀀스")
    private Long id;

    @ApiModelProperty(notes = "머신 타입과 이름")
    private String machineFullName; // 머신Type + 머신Name

    @ApiModelProperty(notes = "구매일")
    private LocalDate datePurchase;


    private MachineItem(MachineItemBuilder builder) {
        this.id = builder.id;
        this.machineFullName = builder.machineFullName;
        this.datePurchase = builder.datePurchase;

    }
    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {

        private final Long id;
        private final String machineFullName; // 머신Type + 머신Name
        private final LocalDate datePurchase;


        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineFullName = machine.getMachineType().getName() + " " + machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
        }

        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }

}
