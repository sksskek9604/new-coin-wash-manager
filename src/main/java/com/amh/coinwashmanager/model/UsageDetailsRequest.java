package com.amh.coinwashmanager.model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailsRequest {

    @ApiModelProperty(notes = "멤버 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "머신 시퀀스")
    private Long machineId;

    @ApiModelProperty(notes = "이용 날짜")
    private LocalDateTime dateUsage;

}
