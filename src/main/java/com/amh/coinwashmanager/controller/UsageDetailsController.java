package com.amh.coinwashmanager.controller;

import com.amh.coinwashmanager.entity.Machine;
import com.amh.coinwashmanager.entity.Member;
import com.amh.coinwashmanager.model.UsageDetailsItem;
import com.amh.coinwashmanager.model.UsageDetailsRequest;
import com.amh.coinwashmanager.model.common.CommonResult;
import com.amh.coinwashmanager.model.common.ListResult;
import com.amh.coinwashmanager.service.MachineService;
import com.amh.coinwashmanager.service.MemberService;
import com.amh.coinwashmanager.service.common.ResponseService;
import com.amh.coinwashmanager.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {
    private final MemberService memberService;
    private final MachineService machineService;
    private final UsageDetailsService usageDetailsService;

    @ApiOperation(value = "이용 내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request) {
        Member member = memberService.getMemberData(request.getMemberId());
        Machine machine = machineService.getMachineData(request.getMachineId());

        usageDetailsService.setUsageDetails(member, machine, request.getDateUsage());
        // 1. dateusage는 박스에서 꺼내서 주면 되죠.
        return ResponseService.getSuccessResult();
        // 2. 이용 내역 조작하면 안되겠죠? (건조기랑 세탁기랑 가격 다른데! - 그럼 U가 필요할까?)
        // 3. 즉 설계할 때 필요한지 안한지, '용도'를 확인하라. 무엇이든 CRUD가 있다? 아님. 필요에 따라 사용하는 것.
        // 4. 이제부터 시작일부터 종료일 두개를 '필수'로 받아서, 거기의 리스트만 출력 받을 것이다. 왜냐하면, 이용내역 같은 경우는 데이터가
        // 너무 많기 때문에, 전체 다 주는 것이 아님. 조회할 때마다 서버가 터지겠죠.
        // 그러니까 시작일/종료일만 지정해서 받아주는 것이 좋음 (= 이 '기간 필터'라고 부른다.)
    }
    // get은 바디가 없다 = 박스가 없다. but 재가공을 하는 과정에서의 모델은 필요하다.
    @ApiOperation(value = "이용내역 리스트")
    @GetMapping("/search")
    public ListResult<UsageDetailsItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd // 기간필터
    ) {
        return ResponseService.getListResult(usageDetailsService.getUsageDetailsAll(dateStart, dateEnd), true);
    }
}

