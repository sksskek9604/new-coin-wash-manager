package com.amh.coinwashmanager.controller;

import com.amh.coinwashmanager.enums.MachineType;
import com.amh.coinwashmanager.model.common.CommonResult;
import com.amh.coinwashmanager.model.common.ListResult;
import com.amh.coinwashmanager.model.common.SingleResult;
import com.amh.coinwashmanager.model.machine.MachineDetail;
import com.amh.coinwashmanager.model.machine.MachineItem;
import com.amh.coinwashmanager.model.machine.MachineNameUpdateRequest;
import com.amh.coinwashmanager.model.machine.MachineRequest;
import com.amh.coinwashmanager.service.MachineService;
import com.amh.coinwashmanager.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기계 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @ApiOperation(value = "기계 정보 등록")
    @PostMapping("/new")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MachineDetail> getMachine(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));

    }
    // pathvariable은 무조건받아야하지만 지금은 두개를 합친 상황. 있어도, 없어도 된다라는 뜻의 연결 주석이 필요함.
    // 연결할 때 @RequestParam / required = false (@RequestParam의 기본값은 required = true이기 때문에 false의 경우를 써줘야함.)
    @ApiOperation(value = "기계 정보 리스트(기본/기계타입별)")
    @GetMapping("/search")
    public ListResult<MachineItem> getMachines(@RequestParam( value = "machineType", required = false) MachineType machineType) {
        if (machineType == null) {
            return ResponseService.getListResult(machineService.getMachines(), true);
        } else return ResponseService.getListResult(machineService.getMachines(machineType), true);
    }

    @ApiOperation(value = "기계 이름만 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @PutMapping("/put-name/{id}")
    public CommonResult putMachineName(@PathVariable  long id, @RequestBody @Valid MachineNameUpdateRequest machineNameUpdateRequest) {
        machineService.putMachineName(id, machineNameUpdateRequest);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "기계 정보 전체수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    })
    @PutMapping("/put-machine-info/{id}")
    public CommonResult putMachine(@PathVariable long id, @RequestBody @Valid MachineRequest machineRequest) {
        machineService.putMachine(id, machineRequest);
        return ResponseService.getSuccessResult();

    }
    //Todo {id} 같은 경로일 경우, id의 경로를 명시하거나, 뒤에 경로에서 구분된다는 것을 꼭 명시해줘야 한다. (1차 오류 발생)
    //Todo @RequestBody와 같이 ()안에 쓰지 않을 경우, 내용상의 문제가 없더라도 오류가 발생한다. swagger에서 입력 모양 확인할 것(2차 오류)


}
