package com.amh.coinwashmanager.controller;


import com.amh.coinwashmanager.model.common.CommonResult;
import com.amh.coinwashmanager.model.common.ListResult;
import com.amh.coinwashmanager.model.common.SingleResult;
import com.amh.coinwashmanager.model.member.MemberItem;
import com.amh.coinwashmanager.model.member.MemberJoinRequest;
import com.amh.coinwashmanager.service.MemberService;
import com.amh.coinwashmanager.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")

public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest request) {
        memberService.setMember(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "회원 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<MemberItem> getMemberOne(@PathVariable long id) {
        return ResponseService.getSingleResult(memberService.getMemberOne(id));
    }

    @ApiOperation(value = "회원 정보 전체 or 유효하다면 보여준다. 조건이 들어간 전체 리스트")
    // Q1. 유효하지 않으면 실패했습니다는 어디에 기능이 있지? - 후에 찾아볼 것
    // Q2. 그리고 어차피 똑같이 들고오는데 왜 두개 서비스가 각자 필요하지?
    // A1. 회원 보는용이랑 관리자 보는 용이 달라서? - 그럴려면 회원에게 다 보여주지 않을 MODEL 별도 필요했을 것
    // A2. 그렇기때문에 '누가 보느냐'보다는 '어떤 기능'인지에 초점을 맞출 것.
    // TRUE FALSE일 경우에 따라 보여지는게 달라진다. 탈퇴한 사람, 탈퇴하지 않은 사람
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(@RequestParam( value = "memberType", required = false) Boolean isEnable) {
        if (isEnable == null) {
            return ResponseService.getListResult(memberService.getMembersAll(), true);
        } else return ResponseService.getListResult(memberService.getMembersEnable(isEnable), true);
    }

    //지운척하는 것 -> put 서비스에게 넘기기
    @ApiOperation(value = "회원 탈퇴")
    @DeleteMapping("/withdrawl-member/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
    // 경로 구분 안해주면 100% 오류 나서 고생하니까 꼭!! 경로 구분해줍니다.(경험담)

    @ApiOperation(value = "회원 정보 수정")
    @PutMapping("/put-member-info/{id}")
    public CommonResult putMemberInfo(@PathVariable long id, @RequestBody @Valid MemberJoinRequest request) {
        memberService.putMemberInfo(id, request);
        return ResponseService.getSuccessResult();
    }

}
