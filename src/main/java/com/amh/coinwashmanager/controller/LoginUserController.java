package com.amh.coinwashmanager.controller;

import com.amh.coinwashmanager.model.common.CommonResult;
import com.amh.coinwashmanager.model.common.SingleResult;
import com.amh.coinwashmanager.model.loginuser.LoginUserItem;
import com.amh.coinwashmanager.model.loginuser.LoginUserRequest;
import com.amh.coinwashmanager.service.LoginUserService;
import com.amh.coinwashmanager.service.common.ResponseService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "로그인 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/loginuser")
public class LoginUserController {

    private final LoginUserService loginUserService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/join")
    public CommonResult setLoginUser(@RequestBody @Valid LoginUserRequest request) {
        loginUserService.setLoginUser(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 로그인")
    @PostMapping("/login")
    public SingleResult setLoginUserLogin(@RequestBody @Valid LoginUserRequest request) {
        return ResponseService.getSingleResult(loginUserService.setLoginUserDoLogin(request));
    }

    @ApiOperation(value = "로그인 회원정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "로그인회원 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<LoginUserItem> getLoginUser(@PathVariable long id) {
        return ResponseService.getSingleResult(loginUserService.getLoginUser(id));
    }
}
