# 코인빨래방 관리 API

* 코인빨래방 운영 시 필요한 기능을 담은 회원/기계/이용내역 관리 API입니다.

---
###
### LANGUAGE
> ##### - JAVA 16
> ##### - SpringBoot 2.7.3
###

### 기능
> - 로그인
> > 1. 회원 로그인
> * 회원 관리
> > 회원 정보 등록
> 
> > 회원 수정
> > > 1. 회원 정보 수정
> > > 2. 회원 탈퇴
> >회원 정보 가져오기
> > > 1. 회원 정보 가져오기
> > > 2. 탈퇴하지 않은 회원 정보 리스트
>
> * 기계 관리
> > 기계 정보 등록
> 
> > 기계 정보 가져오기
> 
> > 기계 정보 리스트(기본/기계 타입별)
> 
> > 기계 정보 수정
> > > 1. 기계 이름 수정
> > > 2. 기계 정보 전체 수정
> * 이용 내역 관리
> > 이용 내역 등록
> 
> > 이용 내역 리스트


---

## Swagger
> ![Swagger](./images/swagger.png)
>
## 로그인 API
>
> ![LoginAPI](./images/login_api.png)
## 기계관리 API
>
> ![MachineAPI](./images/machine_api.png)
## 회원관리 API
>
> ![MemberAPI](./images/member_api.png)
## 이용내역 API
>
> ![UsageDetailsAPI](./images/usage_details_api.png)
